﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName="Brains/Player Controlled")]
public class PlayerControlledTank : TankBrain
{

	public int PlayerNumber;
	private string m_MovementAxisName;
	private string m_TurnAxisName;
	private string m_FireButton;
	private string m_RamButton;
	private string m_SwapAmmoButton;


	public void OnEnable()
	{
		m_MovementAxisName = "Vertical" + PlayerNumber;
		m_TurnAxisName = "Horizontal" + PlayerNumber;
		m_FireButton = "Fire" + PlayerNumber;
		m_RamButton = "Ram" + PlayerNumber;
		m_SwapAmmoButton = "SwapAmmo" + PlayerNumber;
	}

	public override void Think(TankThinker tank)
	{
		var movement = tank.GetComponent<TankMovement>();

		movement.Steer(Input.GetAxis(m_MovementAxisName), Input.GetAxis(m_TurnAxisName));

		var shooting = tank.GetComponent<TankShootingManager>();

		if (Input.GetButton(m_FireButton))
			shooting.BeginChargingShot();
		else
			shooting.FireChargedShot();

        if (Input.GetButton(m_RamButton))
        {
            tank.GetComponent<TankRam>().BeginRam();
        }
        if (Input.GetButtonDown(m_SwapAmmoButton))
        {
            tank.GetComponent<TankShootingManager>().m_TankShellProvider.CycleAmmo();
        }
	}
}
