﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Provides tracked shells
/// </summary>
public class TankShellProvider : ITankShellProvider
{
    private readonly int m_MaxShells;
    private readonly Slider m_AmmoSlider;
    private readonly Image m_AmmoSliderImage;
    private List<TankShellData> m_TankShells;
    private int m_CurrentShellIndex = 0;
    private TankShellData CurrentShellData => m_TankShells[m_CurrentShellIndex];
    /// <summary>
    /// Whether the current ammo type has shells
    /// </summary>
    public bool HasShells => CurrentShellData.ShellsRemaining > 0;

    public TankShellProvider(int maxShells, Slider ammoSlider, Image ammoSliderImage)
    {
        m_MaxShells = maxShells;
        m_AmmoSlider = ammoSlider;
        m_AmmoSliderImage = ammoSliderImage;
        m_TankShells = new List<TankShellData>
        {
            new TankShellData(Color.red, maxShells, typeof(BasicShell)),
            new TankShellData(Color.yellow, maxShells, typeof(ConfusionShell)),
            new TankShellData(Color.green, maxShells, typeof(MineShell)),
            new TankShellData(Color.cyan, maxShells, typeof(BouncyShell))
        };
        m_AmmoSliderImage.color = CurrentShellData.ShellColor;
    }

    /// <summary>
    /// Reload ammo for all shell types
    /// </summary>
    public void ReloadAllAmmo()
    {
        foreach (var tankShell in m_TankShells)
        {
            tankShell.ShellsRemaining = m_MaxShells;
        }
        m_AmmoSlider.value = CurrentShellData.ShellsRemaining;
    }

    /// <summary>
    /// Cycle to the next type of ammo
    /// </summary>
    public void CycleAmmo()
    {
        m_CurrentShellIndex = m_CurrentShellIndex != m_TankShells.Count - 1 ? m_CurrentShellIndex + 1 : 0;
        m_AmmoSlider.value = CurrentShellData.ShellsRemaining;
        m_AmmoSliderImage.color = CurrentShellData.ShellColor;
    }
    
    /// <summary>
    /// Get a shell from the current ammo type
    /// </summary>
    /// <returns>The type of shell to be fired</returns>
    public Type GetShell()
    {
        CurrentShellData.ShellsRemaining--;
        m_AmmoSlider.value = CurrentShellData.ShellsRemaining;
        return CurrentShellData.ShellType;
    }
}
