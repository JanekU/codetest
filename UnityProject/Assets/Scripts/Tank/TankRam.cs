﻿using System;
using System.Collections;
using UnityEngine;

public class TankRam : MonoBehaviour, IRammable
{
    private const float Duration = 0.3f;
    private const float Speed = 36f;
    private const float RamRadius = 1f;
    private const float RamForce = 2500f;
    private const float RamDamage = 15f;
    private const float RamCooldownDuration = 3f;
    
    private Rigidbody m_Rigidbody;
    private bool m_Ramming;
    private bool m_OnCooldown;
    /// <summary>
    /// Whether this tank can ram currently
    /// </summary>
    public bool CanRam => !m_Ramming && !m_OnCooldown;
    private Coroutine m_RamCoroutine;
    private Coroutine m_RamCooldownCoroutine;
    /// <summary>
    /// Fired when a ram has successfully landed on a target
    /// </summary>
    public event Action RamHitTarget;
    
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    private void OnDisable()
    {
        if (m_RamCoroutine != null)
        {
            StopCoroutine(m_RamCoroutine);
        }

        if (m_RamCooldownCoroutine != null)
        {
            StopCoroutine(m_RamCooldownCoroutine);
        }
    }

    /// <summary>
    /// Begins the ram, if in a valid state
    /// </summary>
    public void BeginRam()
    {
        if (CanRam)
        {
            m_RamCoroutine = StartCoroutine(PerformRam());
        }
    }

    private IEnumerator PerformRam()
    {
        var timeElapsed = 0f;
        m_Ramming = true;
        while (timeElapsed < Duration)
        {
            timeElapsed += Time.deltaTime;
            Vector3 movement = transform.forward * Speed * Time.deltaTime;
            m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
            yield return new WaitForEndOfFrame();
        }
        StopRamming();
    }

    private IEnumerator RamCooldown()
    {
        m_OnCooldown = true;
        yield return new WaitForSeconds(RamCooldownDuration);
        m_OnCooldown = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!m_Ramming)
        {
            return;
        }

        var collider = other.collider;
        var rammable = collider.GetComponent<IRammable>();
        if (rammable != null)
        {
            rammable.GetRammed(RamForce, transform.position, RamRadius);
            RamHitTarget?.Invoke();
            collider.SendMessage("TakeDamage", RamDamage, SendMessageOptions.DontRequireReceiver);
            StopRamming();
        }
    }

    private void StopRamming()
    {
        m_Ramming = false;
        m_RamCooldownCoroutine = StartCoroutine(RamCooldown());
    }

    /// <summary>
    /// Ram this object
    /// </summary>
    /// <param name="ramForce">The force of the impact</param>
    /// <param name="location">The location of the object ramming this</param>
    /// <param name="ramRadius">The radius of the ramming object</param>
    public void GetRammed(float ramForce, Vector3 location, float ramRadius)
    {
        m_Rigidbody.AddExplosionForce(ramForce, location, ramRadius);
    }
}
