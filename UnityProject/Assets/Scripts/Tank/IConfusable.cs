﻿public interface IConfusable
{
    /// <summary>
    /// Confuse this object
    /// </summary>
    /// <param name="duration">The duration the confusion should last</param>
    void Confuse(float duration);
}
