﻿using UnityEngine;
/// <summary>
/// Handles charging aiming 
/// </summary>
public class TankAimCharger : ITankAimCharger
{
    private readonly float m_MinLaunchForce;
    private readonly float m_MaxLaunchForce;
    /// <summary>
    /// Is the aim currently being charged
    /// </summary>
    public bool IsCharging { get; set; }
    /// <summary>
    /// The current launch force of the aim
    /// </summary>
    public float LaunchForce { get; private set; }
    private float m_ChargeSpeed;

    public TankAimCharger(float minLaunchForce, float maxLaunchForce, float maxChargeTime)
    {
        m_MinLaunchForce = minLaunchForce;
        m_MaxLaunchForce = maxLaunchForce;
        LaunchForce = minLaunchForce;
        m_ChargeSpeed = (maxLaunchForce - minLaunchForce) / maxChargeTime;
    }
    
    /// <summary>
    /// Attempts to begin the process of charging a shot
    /// </summary>
    /// <returns>TRUE if process started successfully, FALSE if process start failed</returns>
    public bool TryBeginChargingShot()
    {
        if (IsCharging)
        {
            return false;
        }

        LaunchForce = m_MinLaunchForce;
        IsCharging = true;
        return true;
    }

    /// <summary>
    /// Updates the force value of the charge linearly 
    /// </summary>
    /// <param name="timeSinceLastUpdate">The time since this function was last called</param>
    /// <returns>The current force value</returns>
    public float UpdateForceValue(float timeSinceLastUpdate)
    {
        LaunchForce = IsCharging
            ? Mathf.Min(m_MaxLaunchForce, LaunchForce + m_ChargeSpeed * timeSinceLastUpdate)
            : m_MinLaunchForce;
        return LaunchForce;
    }

    /// <summary>
    /// Resets the launch force back to the start
    /// </summary>
    public void ResetLaunchForce()
    {
        LaunchForce = m_MinLaunchForce;
    }
}
