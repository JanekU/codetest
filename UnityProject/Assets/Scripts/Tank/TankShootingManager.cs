﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TankShootingManager : MonoBehaviour
{
    /// <summary>
    /// Handles charging up the aim
    /// </summary>
    public ITankAimCharger m_TankAimCharger;
    /// <summary>
    /// Provides shells to shoot
    /// </summary>
    public ITankShellProvider m_TankShellProvider;
    /// <summary>
    /// A workaround for shells requiring serialized fields
    /// </summary>
    private IShellFactory m_ShellFactory;        
    public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
    public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
    public Slider m_AmmoSlider;                 // Displays the current ammo
    public Image m_AmmoImage;                   // The image of the ammo display
    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
    public AudioClip m_MisfireClip;                // Audio that plays when each shot is fired.
    public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
    public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
    public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.
    private const int MaxShells = 4;
	public bool IsCharging => m_TankAimCharger.IsCharging;
	public bool CurrentAmmoTypeHasShellsLeft => m_TankShellProvider.HasShells;

    private void OnEnable()
    {
        m_ShellFactory = FindObjectOfType<ShellFactory>();
        m_TankAimCharger = new TankAimCharger(m_MinLaunchForce, m_MaxLaunchForce, m_MaxChargeTime);
        m_TankShellProvider = new TankShellProvider(MaxShells, m_AmmoSlider, m_AmmoImage);
        m_AimSlider.value = m_TankAimCharger.LaunchForce;
        GetComponent<TankRam>().RamHitTarget += m_TankShellProvider.ReloadAllAmmo;
    }

    private void OnDisable()
    {
        GetComponent<TankRam>().RamHitTarget -= m_TankShellProvider.ReloadAllAmmo;
    }

    public void BeginChargingShot()
	{
		var canCharge = m_TankAimCharger.TryBeginChargingShot();
        if (canCharge)
        {
            // Change the clip to the charging clip and start it playing.
            m_ShootingAudio.clip = m_ChargingClip;
            m_ShootingAudio.Play();
        }
    }

	public void FireChargedShot()
	{
        if (!m_TankAimCharger.IsCharging)
        {
            return;
        }

		Fire();
        m_TankAimCharger.IsCharging = false;
    }

    private void Update()
	{
        m_AimSlider.value = m_TankAimCharger.UpdateForceValue(Time.deltaTime);
    }


    private void Fire ()
    {
        if (m_TankShellProvider.HasShells)
        {
            var shellType = m_TankShellProvider.GetShell();
            var shellPrefab = m_ShellFactory.GetShell(shellType);
            var shellInstance = Instantiate (shellPrefab, m_FireTransform.position, m_FireTransform.rotation).GetComponent<Rigidbody>();
            shellInstance.velocity = m_TankAimCharger.LaunchForce * m_FireTransform.forward; 
            m_ShootingAudio.clip = m_FireClip;
        }
        else
        {
            m_ShootingAudio.clip = m_MisfireClip;
        }

        m_ShootingAudio.Play ();
        m_TankAimCharger.ResetLaunchForce();
    }
}