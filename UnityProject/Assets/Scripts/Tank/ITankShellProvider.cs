using System;

public interface ITankShellProvider
{
    /// <summary>
    /// Whether the current ammo type has shells
    /// </summary>
    bool HasShells { get; }

    /// <summary>
    /// Reload ammo for all shell types
    /// </summary>
    void ReloadAllAmmo();

    /// <summary>
    /// Cycle to the next type of ammo
    /// </summary>
    void CycleAmmo();

    /// <summary>
    /// Get a shell from the current ammo type
    /// </summary>
    /// <returns>The type of shell to be fired</returns>
    Type GetShell();
}