public interface ITankAimCharger
{
    /// <summary>
    /// Is the aim currently being charged
    /// </summary>
    bool IsCharging { get; set; }

    /// <summary>
    /// The current launch force of the aim
    /// </summary>
    float LaunchForce { get; }

    /// <summary>
    /// Attempts to begin the process of charging a shot
    /// </summary>
    /// <returns>TRUE if process started successfully, FALSE if process start failed</returns>
    bool TryBeginChargingShot();

    /// <summary>
    /// Updates the force value of the charge linearly 
    /// </summary>
    /// <param name="timeSinceLastUpdate">The time since this function was last called</param>
    /// <returns>The current force value</returns>
    float UpdateForceValue(float timeSinceLastUpdate);

    /// <summary>
    /// Resets the launch force back to the start
    /// </summary>
    void ResetLaunchForce();
}