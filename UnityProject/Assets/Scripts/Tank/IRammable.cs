using UnityEngine;

public interface IRammable
{
    /// <summary>
    /// Ram this object
    /// </summary>
    /// <param name="ramForce">The force of the impact</param>
    /// <param name="location">The location of the object ramming this</param>
    /// <param name="ramRadius">The radius of the ramming object</param>
    void GetRammed(float ramForce, Vector3 location, float ramRadius);
}