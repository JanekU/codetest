﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

//Taken from: https://stackoverflow.com/a/6944605
//Modified to return types
public static class ReflectiveEnumerator
{
    public static IEnumerable<Type> GetEnumerableOfType<T>(params object[] constructorArgs) where T : class
    {
        List<Type> objects = Assembly.GetAssembly(typeof(T))
            .GetTypes()
            .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T)))
            .ToList();
        objects.Sort();
        return objects;
    }
}
