﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Handles data around a shell
/// </summary>
public class TankShellData
{
    /// <summary>
    /// The type of shell this is
    /// </summary>
    public Type ShellType;
    /// <summary>
    /// The number of shells of this type remaining
    /// </summary>
    public int ShellsRemaining;
    /// <summary>
    /// The colour that represents this shell
    /// </summary>
    public readonly Color ShellColor;

    public TankShellData(Color shellColor, int shellsRemaining, Type shellType)
    {
        Assert.IsTrue(shellType.IsSubclassOf(typeof(Shell)), "Class is not a shell.");
        ShellColor = shellColor;
        ShellsRemaining = shellsRemaining;
        ShellType = shellType;
    }
}
