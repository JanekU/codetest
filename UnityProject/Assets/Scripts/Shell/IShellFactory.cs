using System;

public interface IShellFactory
{
    /// <summary>
    /// Get a shell for a given type
    /// </summary>
    /// <param name="shellType">The type of shell to get</param>
    /// <returns>A prefab of the shell type</returns>
    Shell GetShell(Type shellType);
}