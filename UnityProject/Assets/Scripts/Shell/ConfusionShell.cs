﻿using UnityEngine;
/// <summary>
/// Confuses any targets hit
/// </summary>
public class ConfusionShell : Shell
{
    private const float ConfusionDurationModifier = 3;
    private void OnCollisionEnter(Collision other)
    {
        Explode();
    }

    protected override void AdditionalExplosionAffect(Collider collider)
    {
        collider.GetComponent<IConfusable>()?.Confuse(CalculateExplosionStrength(collider)*ConfusionDurationModifier);
    }
}
