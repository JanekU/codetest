﻿using System.Collections;
using System.Linq;
using UnityEngine;
/// <summary>
/// Lays a mine that explodes if something gets near
/// </summary>
public class MineShell : Shell
{
    /// <summary>
    /// The clip to play when a targetable object is within range
    /// </summary>
    public AudioClip m_ArmClip;
    private const float ArmRadius = 4f;
    private const float ArmTime = 0.3f;
    private void OnCollisionEnter(Collision other)
    {
         GetComponent<Rigidbody>().velocity = Vector3.zero;
        StartCoroutine(Arm());
    }

    private IEnumerator Arm()
    {
        var armed = false;
        while (!armed)
        {
            if (GameObject
                .FindGameObjectsWithTag("Player")
                .Any(x => Vector3.Distance(x.transform.position, transform.position) <= ArmRadius))
            {
                armed = true;
                m_ExplosionAudio.clip = m_ArmClip;
                m_ExplosionAudio.Play();
                yield return new WaitForSeconds(ArmTime);
                m_ExplosionAudio.clip = m_ExplosionAudioClip;
                Explode();
                //Quick workaround for the mine using multiple renderers
                GetComponentsInChildren<Renderer>().ToList().ForEach(x => x.enabled = false);
            }
            yield return null;
        }
    }
}
