﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Workaround class for shells components requiring scriptable objects
/// </summary>
public class ShellFactory : MonoBehaviour, IShellFactory
{
    [SerializeField] private List<Shell> shells;

    /// <summary>
    /// Get a shell for a given type
    /// </summary>
    /// <param name="shellType">The type of shell to get</param>
    /// <returns>A prefab of the shell type</returns>
    public Shell GetShell(Type shellType)
    {
        return shells.First(x => shellType == x.GetType());
    }
}
