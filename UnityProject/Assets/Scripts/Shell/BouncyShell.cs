﻿using UnityEngine;

/// <summary>
/// Bounces on impact, only explodes if it hits a player
/// </summary>
public class BouncyShell : Shell
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Explode();
        }
    }
}
