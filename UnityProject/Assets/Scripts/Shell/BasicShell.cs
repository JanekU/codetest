﻿using UnityEngine;
/// <summary>
/// A basic shell, explodes on impact with surface
/// </summary>
public class BasicShell : Shell
{
    private void OnCollisionEnter(Collision other)
    {
        Explode();
    }
}
